from django.db.models import Max

from django_datacenter_manager_main.django_datacenter_manager_main.models import *
from django_mdat_customer.django_mdat_customer.models import *


class DcVlan(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, db_column="customer")
    vlan_id = models.IntegerField()
    comment = models.CharField(max_length=200, blank=True, null=True)
    instance = models.ForeignKey(DcInstance, models.DO_NOTHING)
    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"VLAN {self.vlan_id} - {self.customer.name} [{self.comment}]"

    def save(self, *args, **kwargs):
        # update date_changed
        self.date_changed = timezone.now()

        super(DcVlan, self).save(*args, **kwargs)

    class Meta:
        db_table = "dc_vlan"
        unique_together = (("vlan_id", "instance"),)


class DcVlanQinQ(models.Model):
    instance = models.ForeignKey(DcInstance, models.DO_NOTHING)
    svlan = models.ForeignKey(DcVlan, models.DO_NOTHING, related_name="qinq_svlan")
    cvlan = models.ForeignKey(DcVlan, models.DO_NOTHING, related_name="qinq_cvlan")
    unit = models.IntegerField(default=0)
    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"SVLAN {self.svlan.vlan_id} ({self.svlan.id}) CVLAN {self.cvlan.vlan_id} ({self.cvlan.id})"

    def save(self, *args, **kwargs):
        # update date_changed
        self.date_changed = timezone.now()

        # check if unit must be adjusted
        if self.unit is None or self.unit == 0:
            min_unit = 1000

            max_unit = DcVlanQinQ.objects.filter(
                instance_id=self.instance_id,
            ).aggregate(Max("unit"))["unit__max"]

            if max_unit is None:
                max_unit = 0

            if max_unit < min_unit - 1:
                max_unit = min_unit - 1

            self.unit = max_unit + 1

        super(DcVlanQinQ, self).save(*args, **kwargs)

    class Meta:
        db_table = "dc_vlan_qinq"
        unique_together = (
            ("svlan", "cvlan"),
            ("instance", "unit"),
        )
